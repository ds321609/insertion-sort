using InsertionSort;
using System;
using System.Collections.Generic;
using Xunit;

namespace InsertionSortUnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void InsertionSortTests()
        {
            var unsortedList = new List<int>() { 1, 4, 2, 8, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92 };
            var sortedList = new List<int>() { 1, 4, 2, 8, 345, 123, 43, 32, 5643, 63, 123, 43, 2, 55, 1, 234, 92 };
            sortedList.Sort();
            var sut = new InsertionSortClass().Sort(unsortedList);

            Assert.Equal(sortedList, sut);
        }
    }
}
