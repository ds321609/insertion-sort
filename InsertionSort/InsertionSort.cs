﻿using System;
using System.Collections.Generic;

namespace InsertionSort
{
    public class InsertionSortClass
    {
        public List<int> Sort(List<int> unsortedList)
        {
            List<int> sortedList = unsortedList;

            for (int i = 1; i < sortedList.Count; i++)
            {

                for (int j = i; j > 0; j--)
                {
                    int current = sortedList[j];
                    int previous = sortedList[j - 1];

                    if (previous > current)
                    {
                        sortedList[j - 1] = current;
                        sortedList[j] = previous;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return sortedList;
        }
    }
}
