﻿using System;

namespace InsertionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 8, 4, 9, 1, 3, 2, 5, 7, 6 };


            for (int i = 1; i < arr.Length; i++)
            {
                int value = arr[i];
                int place = i;

                while (place > 0 && arr[place - 1] > value)
                {
                    arr[place] = arr[place - 1];
                    place = place - 1;
                }

                arr[place] = value;
            }
            foreach (int elem in arr)
            {
                Console.WriteLine(elem);
            }
            Console.ReadLine();

        }
    }
}
